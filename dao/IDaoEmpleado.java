package dao;

import java.util.List;

import pojo.Empleado;

public interface IDaoEmpleado{

    public void agregarEmpleado(List<Empleado> empleados, Empleado e);
    public void editarEmpleado(List<Empleado> empleados, Empleado e, Empleado empleadoNuevo);
    public boolean eliminarEmpleado(List<Empleado> empleados, Empleado e);
    public Empleado findByCodigo(List<Empleado> empleados, int codigo);
    public Empleado findByApellidos(List<Empleado> empleados, String apellidos);

}