package pojo;

public class Empleado {

    private int codigo;
    private String cedula;
    private String nombres;
    private String apellidos;
    private String fechaIngreso;
    private float salario;
    private float salarioBruto;
    private float salarioNeto;
    private float Inss;
    private float IR;


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public Empleado(int codigo, String cedula, String nombres, String apellidos, String fechaIngreso, float salario) {
        this.codigo = codigo;
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.fechaIngreso = fechaIngreso;
        this.salario = salario;
    }

    public void mostrarDatos(){
        System.out.println("Datos: ");
        System.out.println("Nombres: " + this.nombres);
        System.out.println("Apellidos: " + this.apellidos);
        System.out.println("Codigo: " + this.codigo);
        System.out.println("Cedula: " + this.cedula);
        System.out.println("Fecha de Ingreso: " + this.fechaIngreso);

    }

    public void mostrarDatoF(){
        System.out.println("Datos: ");
        System.out.println("Nombres: " + this.nombres);
        System.out.println("Apellidos: " + this.apellidos);
        System.out.println("Codigo: " + this.codigo);
        System.out.println("Salario Bruto: " + this.salarioBruto);
        System.out.println("INSS: " + this.Inss);
        System.out.println("IR: " + this.IR);
        System.out.println("Salario Neto: " + this.salarioNeto);
    }

    public float getSalarioBruto() {
        return salarioBruto;
    }

    public void setSalarioBruto(float salarioBruto) {
        this.salarioBruto = salarioBruto;
    }

    public float getSalarioNeto() {
        return salarioNeto;
    }

    public void setSalarioNeto(float salarioNeto) {
        this.salarioNeto = salarioNeto;
    }

    public float getInss() {
        return Inss;
    }

    public void setInss(float inss) {
        Inss = inss;
    }

    public float getIR() {
        return IR;
    }

    public void setIR(float iR) {
        IR = iR;
    }

    


}