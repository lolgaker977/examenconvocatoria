package implementacion;

import java.util.List;

import dao.IDaoEmpleado;
import pojo.Empleado;

public class IDaoImplement implements IDaoEmpleado {

    @Override
    public void agregarEmpleado(List<Empleado> empleados, Empleado e) {
            empleados.add(e);

    }

    @Override
    public void editarEmpleado(List<Empleado> empleados, Empleado e, Empleado empleadoNuevo) {
         for (Empleado empleado : empleados) {
             if(e.getCodigo() == empleado.getCodigo()){
                 eliminarEmpleado(empleados, e);
                 empleados.add(empleadoNuevo);
             }
         }

    }

    @Override
    public boolean eliminarEmpleado(List<Empleado> empleados, Empleado e) {
        for (Empleado empleado : empleados) {
            if(e.getCodigo() == empleado.getCodigo()){
                empleados.remove(empleado);
                return true;
            }
        }
        return false;
    }

    @Override
    public Empleado findByCodigo(List<Empleado> empleados, int codigo) {
        for (Empleado e : empleados) {
            if(codigo == e.getCodigo()){
                return e;
            }
        }

        return null;
    }

    @Override
    public Empleado findByApellidos(List<Empleado> empleados, String apellidos) {
        for (Empleado e : empleados) {
            if(apellidos == e.getApellidos()){
                return e;
            }
        }

        return null;
    }
    
}
