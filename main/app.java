package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import implementacion.IDaoImplement;
import pojo.Empleado;

public class app {
    public static void main(String[] args) {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner = new Scanner(System.in);
        String cedula, nombres = "", apellidos = "", fechaIngreso = "";
        float salario;
        int opc = 0, codigo;
        IDaoImplement iDaoI = new IDaoImplement();
        Empleado empleado;
        List<Empleado> empleados = new ArrayList<>();

        do {
            menu();
            
            opc = scanner.nextInt();

            switch(opc){
                case 1: {
                    System.out.println("Ingrese los datos del empleado:");
                    System.out.print("Codigo: ");
                    codigo = scanner.nextInt();
                    System.out.print("Cedula: ");
                    cedula = scanner.nextLine();
                    try {
                        System.out.print("Nombres: ");
                        nombres = scan.readLine();
                        System.out.print("Apellidos: ");
                        apellidos = scan.readLine();
                        System.out.print("Fecha de Ingreso: ");
                        fechaIngreso = scan.readLine();

                    } catch (IOException e) {
                        System.out.println("ERROR AL INGRESAR LOS DATOS");
                    }

                    System.out.print("Salario: ");
                    salario = scanner.nextFloat();

                    empleado = new Empleado(codigo, cedula, nombres, apellidos, fechaIngreso, salario);
                    iDaoI.agregarEmpleado(empleados, empleado);
                    empleado = null;

                }break;
                case 2: {
                    
                }break;
                case 3 : {
                    System.out.println("Ingrese el codigo del empleado que desea eliminar");
                    System.out.print("Codigo: ");
                    codigo = scanner.nextInt();

                    empleado = iDaoI.findByCodigo(empleados, codigo);

                    if(iDaoI.eliminarEmpleado(empleados, empleado)){
                        System.out.println("Empleado Eliminado con Exito");
                    }

                    empleado = null;
                }break;
                case 4: {
                    int opc2;

                    System.out.println("Elija el parametro por buscar");
                    System.out.println("1.Por Codigo");
                    System.out.println("2.Por Apellidos");
                    System.out.print("Opcion: ");
                    opc2 = scanner.nextInt();

                    switch(opc2){
                        case 1: {
                            System.out.print("Ingrese el codigo del empleado: ");
                            codigo = scanner.nextInt();
                            System.out.println("Buscando por codigo...");
                            empleado = iDaoI.findByCodigo(empleados, codigo);

                            if(empleado == null){
                                System.out.println("No se ha encontrado un empleado con ese codigo!!");
                                break;
                            }else{
                                empleado.mostrarDatos();
                            }

                            empleado = null;
                        }break;
                        case 2: {
                            System.out.print("Ingrese los apellidos del Empleado");
                            try {
                                apellidos = scan.readLine();

                                System.out.println("Buscando por apellidos...");
                                empleado = iDaoI.findByApellidos(empleados, apellidos);

                                if(empleado == null){
                                    System.out.println("No se ha encontrado un empleado con esos apellidos!!");
                                    break;
                                }else{
                                    empleado.mostrarDatos();
                                }

                            } catch (IOException e) {
                                System.out.println("Error al ingresar los datos!!");
                            }

                            empleado = null;
                        }break;
                    }
                }break;
                case 5: {
                    System.out.println("Mostrando Empleados: ");
                    for (Empleado empleado2 : empleados) {
                        calcularInss(empleado2);
                        System.out.println();
                        empleado2.mostrarDatoF();
                    }
                }break;
                case 6: {
                    break;
                }
            }


        }while(opc != 6);

        scanner.close();
        }

        public static void menu(){
            System.out.println("Bienvenido al Programa, Elija una opcion");
            System.out.println("1.Agregar Empleado");
            System.out.println("2.Editar Empleado");
            System.out.println("3.Eliminar Empleado");
            System.out.println("4.Buscar Empleado");
            System.out.println("5.Mostrar Empleados");
            System.out.println("6.Salir");
            System.out.print("Opcion: ");
        }

        public static void calcularInss(Empleado e){
            float salarioAnual = e.getSalario();
            salarioAnual *= 12;
            float IR = 0, inss, salarioBruto, salarioNeto, exceso, IRTotal = 0;
            inss = salarioAnual * 0.0625f;
            salarioBruto = salarioAnual - inss;
            

            if(salarioBruto < 100000.00f){
                IR = 0.0f;
            }else if(salarioBruto > 100000.00f && salarioBruto <= 200000.00f){
                IR = 0.15f;
                exceso = salarioBruto - 100000.00f;
                IRTotal = exceso * IR;

            }else if(salarioBruto > 200000.00f && salarioBruto <= 350000.00f){
                IR = 0.20f;
                exceso = salarioBruto - 200000.00f;
                IRTotal = exceso * IR;
            }else if(salarioBruto > 350000.00f && salarioBruto <= 500000.00f){
                IR = 0.25f;
                exceso = salarioBruto - 350000.00f;
                IRTotal = exceso * IR;
            }else if(salarioBruto > 500000.01f){
                IR = 0.30f;
                exceso = salarioBruto - 500000.01f;
                IRTotal = exceso * IR;
            }

            salarioNeto = salarioBruto - IRTotal;

            e.setSalarioBruto(salarioBruto);
            e.setIR(IRTotal);
            e.setInss(inss);
            e.setSalarioNeto(salarioNeto);

        }
}
